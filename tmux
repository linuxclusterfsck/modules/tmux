#!/usr/bin/env bash

if [[ $(type -t cleanup_tmux) ]]
  ## Unsets tmux before defining a new version if it is reloaded.
  then
  cleanup_tmux
fi

bot_pane()
{
  tm_buffer="$1"
  watch_chat="$2"
  lib_text="${watch_chat%/*/*}"
  session="$bot_nick"
  outfile="$botdir/$channel"
  server="$botdir/server"
  touch "$outfile"

  sleep 1s

  tmux kill-session -t "$session" 2>/dev/null
  tmux -2 new-session -d -s "$session" tail -n "$loglimit" -f "$server" 2>/dev/null
  tmux select-window -t "$session":0 2>/dev/null
  if [[ -x "$watch_chat" ]]
  then
    tmux -2 split-window -v -p 80 "$watch_chat" "$lib_text" 2>/dev/null
  fi
  if [[ -x "$tm_buffer" ]]
  then
    tmux -2 split-window -v -l 3 "$tm_buffer"
  fi
  tmux set-option mouse on
  tmux set-option status off
  tmux -2 a -t "$session"
  tmux set-option -u mouse
  tmux set-option -u status
}
tmuxfunctions+=("bot_pane")

cleanup_tmux()
## Clean up everything to do with the tmux module.
{
  ## Unset the functions.
  tmuxfunctions+=("cleanup_tmux")
  for function in "${tmuxfunctions[@]}"
  do
    unset -f "$function"
  done
  ## Unset the variables. "_" contains us running cleanup_tmux
  ## We clean up so the terminal is exactly the same as if you had
  ## opened a new one.
  tmuxvariables+=("_" "var")
  for var in "${tmuxvariables[@]}"
  do
    unset -v "$var"
  done
}
